import numpy as np 
import pandas as pd 
import tensorflow as tf 
import math
from sklearn.preprocessing import StandardScaler

class glScale():
    """
        Fit a logistic curve to the cumulative density function of the data and apply it.
        Final Values range from 0 to 1 and are robust to outliers.
    """
    available_methods = ['tensor']
    def __init__(self,learning_rate = 0.1,epochs = 100,verbose_frequence=False,batch=1,sampled=False,sample_size=0.25,random_state=42):
        self.learning_rate = learning_rate
        self.epochs = epochs
        self.verbose_frequence = verbose_frequence #frequency in epochs to verbose status of training
        self.batch = batch #size of batch
        self.error = [] #saves the error of each epoch
        self.sampled = sampled
        self.sample_size = sample_size
        self.random_state = random_state

    def fit(self,data):
        
        if self.sampled == True:
            ## Sample data to fit
            data = data.sample(frac=self.sample_size,random_state=self.random_state)

        #Pre scale to avoid tensorflow overflow
        data = pd.DataFrame(data)        
        self.pre_scaler = StandardScaler()
        data = pd.Series([value for i in self.pre_scaler.fit_transform(data) for value in i])
        
        max_value = data.max()
        #print(max_value)
        min_value = data.min()
        
        tf.reset_default_graph() #reset tensorboard graph

        #generate cumulative density function (cdf)
        self.cdf = []
        size = len(data)
        for i,value in enumerate(data):
            #print(i)
            #print(value)
            self.cdf.append(data[data<=value].count()/size)
        #calculate initial LG parameters
        
        M0 = data.iloc[self.cdf.index(min(self.cdf, key=lambda x:abs(x-0.5)))]
        #print(M0)
        #find value of Px^-1(0.5): first find index where value is closest to 0.5 than get the value from the raw data
        Q0 = 1.000000 ##!!!NEED TO WORK ON THIS INITIAL VALUE
        v0 = math.log(Q0+1,2)
        B0 = (math.log( math.pow((1+Q0),math.log(10))-1 )-math.log(Q0))/(M0-min_value)

        if B0==float('inf') or B0 == float('-inf'):
            B0=1

        #start building tensorflow
        X = tf.placeholder(tf.float64,name="X")
        Y = tf.placeholder(tf.float64,name="Y")

        M = tf.Variable(M0,dtype=tf.float64,name="M")
        B = tf.Variable(B0,dtype=tf.float64,name="B")
        v = tf.Variable(v0,dtype=tf.float64,name="v")
        Q = tf.Variable(Q0,dtype=tf.float64,name="Q")
       
        a = tf.subtract(X,M)
        b = tf.multiply(a,B)
        c = tf.exp(b)
        d = tf.multiply(Q,c)+1
        e = tf.pow(d,1/v)
        f = 1/e

        cost = tf.losses.mean_squared_error(labels = 1-f,predictions = Y)
        optimizer = tf.train.GradientDescentOptimizer(self.learning_rate).minimize(cost)
        #tb error summary
        cost1 = tf.summary.scalar("cost", cost)
        #accuracy = tf.summary.scalar("accuracy", accuracy)
        #train_summary_op = tf.summary.merge([cost,accuracy])
        
        self.sess = tf.Session()
        init = tf.global_variables_initializer() 
        self.sess.run(init)

        #tensorboard
        writer = tf.summary.FileWriter("./graphs", self.sess.graph)

        #TRAINING
        for i in range(self.epochs):
            #batching
            for j in range(int(len(data)/self.batch)+1):
                if j != int(len(data)/self.batch):
                    self.sess.run(optimizer,feed_dict={X:data[j*self.batch:],Y:self.cdf[j*self.batch:]})
                else:
                    self.sess.run(optimizer,feed_dict={X:data[j*self.batch:(j+1)*self.batch],Y:self.cdf[j*self.batch:(j+1)*self.batch]})
            epoch_error = self.sess.run(cost,feed_dict = {X:data,Y:self.cdf})
            
            loss = self.sess.run(cost1, feed_dict = {X:data,Y:self.cdf})
            writer.add_summary(loss, i)

            self.error.append(epoch_error)

            #verbosing
            if self.verbose_frequence != False:
                if (i+1)%self.verbose_frequence == 0:
                    print('Epoch: '+str(i+1)+' MSError: '+str(epoch_error))
            

        #save results of logistic curve parameters
        self.lg_m = self.sess.run(M)
        self.lg_q = self.sess.run(Q)
        self.lg_v = self.sess.run(v)
        self.lg_b = self.sess.run(B)

    def transform(self,data):
        results_y = []
        data = pd.DataFrame(data)
        data = pd.Series([value for i in self.pre_scaler.transform(data) for value in i])
        for value in data:
            results_y.append(1/(math.pow(1+self.lg_q*math.exp(-self.lg_b*(value-self.lg_m)),1/self.lg_v)))
        return results_y

    def fit_transform(self,data):
        self.fit(data)
        data_transformed = self.transform(data)
        return data_transformed

"""How to benchmark:
        tryout a famous net against other normalizations (z and min-max)
        there is need to retrain when a lot of new values appears? if so, there is need to retrain the neural network? 
            A variable can increase its mean value over time and these value can be caught on the upper side of the logistic function. ex: population salary. over time values will aproximate 1 due to inflation.
"""