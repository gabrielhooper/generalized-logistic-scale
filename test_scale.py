import glScale
import pandas as pd 
import matplotlib.pyplot as plt
import seaborn as sns

#Open data
df = pd.read_csv('./datasets/iris.csv')
sample_size = 1
sampled = True
random_state = 42
#Apply scale
scale = glScale.glScale(epochs=30,learning_rate=1,verbose_frequence=10,batch=37,sampled=sampled,sample_size=sample_size,random_state=random_state)
data = df[df.columns[0]]
results = scale.fit_transform(data*1000)
#print(results)

#visualize scale
plt.figure() 
sns.set_style('whitegrid')
sns.scatterplot(data.sample(frac=sample_size,random_state=random_state),scale.cdf)
sns.scatterplot(data,results,marker='+',color='r')
plt.xlabel('True Value')
plt.ylabel("'Normalized' Value")
plt.show()

plt.figure()
sns.set_style('whitegrid')
sns.scatterplot([i for i in range(len(scale.error))],scale.error)
plt.xlabel('Epoch')
plt.ylabel("MSError")
plt.show()
