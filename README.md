# Generalized Logistic Scale

#implementation of generalized Logistic Scale propose on the paper: 
A robust data scaling algorithm to improve classification accuracies in biomedical data, Xi Hang Cao, Ivan Stojkovic and Zoran Obradovic. BMC Bioinformatics. 2016; 17(1): 359.
Published online 2016 Sep 9. doi: 10.1186/s12859-016-1236-x

Using tensorflow

Exemplo:
![alt text](https://gitlab.com/gabrielhooper/generalized-logistic-scale/raw/master/imgs/Figure_0_25.png "Exemplo de normalização logística")
